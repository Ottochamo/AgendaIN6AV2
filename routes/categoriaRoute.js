var express = require("express");
var categoria = require("../model/categoria");
var Autenticacion = require('../helper/autenticacion');

var auth = Autenticacion();

var router = express.Router();

router.get("/api/Categoria/", function(req, res) {
    categoria.getCategorias(function(error, resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
            res.json({"Mensaje": "No hay Categorías"});
        }
    });
});

router.get("/api/Categorias", function(req, res) {
    //auth.autorizar(req);
    categoria.getMyCategorias(auth.getIdUsuario(), function(error, resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
            res.json({'Mensaje': 'No tienes categorias'});
        }
    });
});

router.get("/api/Categoria/:idCategoria", function(req, res) {
    var idCategoria = req.params.idCategoria;

    categoria.getCategoria(idCategoria, function(error, resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
            res.json({"Mensaje": "No hay Categorías"});
        }
    });
});

router.post("/api/Categoria/", function(req, res) {
    var data = {
        "idUsuario": auth.getIdUsuario(),
        "nombreCategoria": req.body.nombreCategoria
    }

    categoria.insert(data, function(error, resultado) {
        if ( (resultado) && (resultado.insertId) > 0) {
            res.redirect("/api/Categoria");
        } else {
            res.json({"mensaje": "No se ingreso la categoria"});
        }
    });

});

router.put("/api/Categoria/:idCategoria", function(req, res) {
    var data = {
        "idCategoria": req.body.idCategoria,
        "nombreCategoria": req.body.nombreCategoria
    }

    console.log(data)

    var idCategoria = req.params.idCategoria;
    if (idCategoria == data.idCategoria) {
        categoria.update(data, function(error, resultado) {
            if (resultado !== undefined) {
                res.json(resultado);
            } else {
                res.json({"Mensaje": "No se modifico la categoria"});
            }
        });
    } else {
        res.json({"Mensaje": "No concuerdan los datos"});
    }

});

router.delete("/api/Categoria/:idCategoria",
    function(req, res) {
        var idCategoria = req.params.idCategoria;
        categoria.delete(idCategoria, function(error, resultados) {
            if (resultados) {
                res.json({'Mensaje': 'Eliminado'});
            } else {
                res.json({"Mensaje": "No se pudo eliminar"});
            }
        });
});

//npm uninstall pug --save
//npm install ejs --save

module.exports = router;
