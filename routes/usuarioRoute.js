var express = require('express');
var usuario = require('../model/usuario');
var formidable = require('formidable');
var fs = require('fs-extra');
var util = require('util');

var router = express.Router();

router.get('/api/Usuario', function(req, res) {
    usuario.getUsuarios(function(error, resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
            res.json({'Mensaje': 'No hay usuarios'});
        }
    });
});

router.get('/api/Usuario/:idUsuario', function(req, res) {
    var idUsuario = req.params.idUsuario;

    usuario.getUsuario(idUsuario, function(error, resultados) {
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
            res.json({'Mensaje': 'No se encontró al usuario'});
        }
    });
});

router.post("/autenticar", function(req, res) {
    var data = {
        "nick": req.body.nick,
        "contrasena": req.body.contrasena
    };

    usuario.autenticar(data, function(error, resultado) {
        if (resultado !== undefined) {
            res.cookie('idUsuario', resultado[0].idUsuario);
            res.cookie('nick', resultado[0].nick);
            console.log('Se guardo la cookie');

            res.redirect('/');
        } else {
            res.json({'Mensaje': 'No se ingreso el Usuario'});
        }
    });
});

router.post('/registrar', function(req, res, next) {
    
    var form = new formidable.IncomingForm();
 // parse a file upload
    form.parse(req, function(err, fields, files) {
      //res.writeHead(200, {'content-type': 'text/plain'});
      //res.write('Upload received :\n');
      
    //console.log(files.foto.name);

      //res.end(util.inspect({fields: fields, files: files}));
      var imagePath = (files.foto.name == undefined) 
            ? '' : 'images/' + files.foto.name;
      var data = {
          'nick': fields.nick,
          'contrasena': fields.contrasena,
          'foto': imagePath
      }

      console.log(data)
      
      registrarUsuario(data);
      res.redirect('/');
    });
 form.on('end', function(fields, files) {
        if (this.openedFiles[0].name !== '' ) {
             //el this a donde hace referencia?
             /* Temporary location of our uploaded file */
            var temp_path = this.openedFiles[0].path;
            /* The file name of the uploaded file */
            var file_name = this.openedFiles[0].name;
            /* Location where we want to copy the uploaded file */
            var new_location = './public/images/';
            fs.copy(temp_path, new_location + file_name, function(err) {  
                if (err) {
                    console.error(err);
                } else {
                    console.log('success');
                }
            });
        }
        
    });
    return;

});

router.put('/api/Usuario/:idUsuario', function(req, res) {
    var data = {
        'idUsuario': req.body.idUsuario,
        'nick': req.body.nick,
        'contrasena': req.body.contrasena
    };

    usuario.update(data, function(error, resultado) {
        if (resultado.insertId > 0 ) {
            res.redirect('/');
        } else {
            res.json({'Mensaje': 'No se pudo editar el usuario'});
        }
    });

});

router.delete('/api/Usuario/:idUsuario', function(req, res) {
    var idUsuario = req.params.idUsuario;

    usuario.delete(idUsuario, function(error, result) {
        if (result) {
            res.redirect('/api/Usuario/');
        } else {
            res.json({'Mensaje': 'No se pudo eliminar'});
        }
    });

});

function registrarUsuario(data) {

    console.log(data);

        usuario.insert(data, function(error, resultado) {
        if(resultado.insertId > 0) {
            console.log('todo bien');
        } else {
            res.json({'Mensaje': 'No se pudo registrar'});
        }
    });
}


module.exports = router;