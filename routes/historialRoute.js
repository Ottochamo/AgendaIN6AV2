var express = require('express');
var historial = require('../model/historial');
var Autenticacion = require('../helper/autenticacion');

auth = Autenticacion();

var router = express.Router();

router.get('/api/Historial', function(req, res) {
    console.log(auth.getIdUsuario());
    historial.getHistorials(auth.getIdUsuario(), 
    function(error, resultados) {
        console.log(error);
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
            res.json({'Mensaje': 'No hay historiales'});
        }
    });
});

module.exports = router;