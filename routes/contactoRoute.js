var express = require('express');
var contacto = require('../model/contacto');
var Autenticacion = require('../helper/autenticacion');

auth = Autenticacion();

var router = express.Router();

router.get('/api/Contacto/', function(req, res) {
    contacto.getContactos(auth.getIdUsuario(), 
    function(error, resultados) {
        
        if (typeof resultados !== undefined) {
            res.json(resultados);
        } else {
            res.json({'Mensaje': 'No hay contactos'});
        }
    });
});

router.post('/api/Contacto/', function(req, res) {
    var data = {        
        'nombre': req.body.nombre,
        'apellido': req.body.apellido,
        'direccion': req.body.direccion,
        'correo': req.body.correo,
        'telefono': req.body.telefono,
        'idCategoria': req.body.idCategoria
    };

    console.log('idUsuario = ' + auth.getIdUsuario());

    contacto.insert(auth.getIdUsuario(), data, function(error, resultado) {
        if (resultado.insertId > 0) {
            res.json({'mensaje': 'funciona?'})
        } else {
            res.json({"mensaje": "No se ingreso"});
        }
    });

});

router.put('/api/Contacto/:idContacto', function(req, res) {
    var idContacto = req.params.idContacto;

    var data = {
        'idContacto': req.body.idContacto,
        'nombre': req.body.nombre,
        'apellido': req.body.apellido,
        'direccion': req.body.direccion,
        'telefono': req.body.telefono,
        'correo': req.body.correo,
        'idCategoria': req.body.idCategoria
    };

    contacto.update(data, function(error, resultado) {
        if (resultado.insertId > 0 ) {
            res.redirect('/api/Contacto');
        } else {
            res.json({'Mensaje': 'No se pudo editar el usuario'});
        }
    });
    

});

router.delete('/api/Contacto/:idContacto', function(req, res) {
    var idContacto = req.params.idContacto;

    contacto.delete(idContacto, function(error, result) {
        console.log(result);        
        if (result.insertId == 0) {
            res.json({'Mensaje' : 'se elimino'})
        } else {
            res.json({'Mensaje': 'No se pudo eliminar'});
        }
    });

});

module.exports = router;