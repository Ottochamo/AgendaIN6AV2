USE Agendain6av2;

DELIMITER &&
-- call sp_insertarContacto(1,1,'a','a','a','a','a')	


CREATE PROCEDURE sp_InsertarContacto (IN _idUsuario INT, IN _idCategoria INT , IN _nombre VARCHAR(20), 
	IN _apellido VARCHAR(20), IN _direccion VARCHAR(20), IN _telefono VARCHAR(20), 
    IN _correo VARCHAR(20))
	BEGIN
    DECLARE _idContacto INT;
    
    INSERT INTO Contacto(idCategoria, nombre, apellido, 
		direccion, telefono, correo)         
        VALUES(_idCategoria, _nombre, _apellido, _direccion, _telefono, _correo);
        
    
    SET _idContacto = (SELECT MAX(idContacto) FROM Contacto LIMIT 1);
    
    INSERT INTO DetalleUsuario(idUsuario, idContacto)
		VALUES(_idUsuario, _idContacto);
    
    END&&
    
DELIMITER ;    

DROP PROCEDURE sp_insertarContacto;

INSERT INTO detalleusuario(idUsuario, idContacto) VALUES(1,1);

DELIMITER &&
CREATE PROCEDURE sp_InsertarDetalle(_idContacto INT, _
DELIMITER ;

USE agendain6av2;

-- DROP PROCEDURE  sp_insertarcontacto;
DELIMITER $$

CREATE PROCEDURE sp_EditarContacto (IN _idContacto INT, IN _idCategoria INT,  
	IN _nombre VARCHAR(20), IN _apellido VARCHAR(20), 
    IN _direccion VARCHAR(20), IN _telefono VARCHAR(20), IN _correo VARCHAR(20))
	BEGIN
    
    UPDATE Contacto SET idCategoria = _idCategoria, nombre = _nombre, apellido = _apellido,
    direccion = _direccion, telefono = _telefono, correo = _correo
    WHERE idContacto = _idContacto;
    
    UPDATE DetalleUsuario SET idContacto = _idContacto
		WHERE idContacto = _idContacto;
    
    END$$
    
DELIMITER ;

-- DROP PROCEDURE sp_EditarContacto;

DELIMITER $$
CREATE PROCEDURE sp_EliminarContacto (IN _idContacto INT)
	BEGIN
    DELETE FROM Contacto WHERE idContacto = _idContacto;
    END$$

DELIMITER ;


-- Procedimientos Usuario

DELIMITER $$

CREATE PROCEDURE sp_InsertUsuario(IN _nick VARCHAR(30), IN _contrasena VARCHAR(30))
	BEGIN
    INSERT INTO Usuario(nick, contrasena) VALUES(_nick, _contrasena);
    END$$

DELIMITER ;

-- DROP PROCEDURE sp_InsertarDetalleCategoria;

DELIMITER &&
	CREATE PROCEDURE sp_InsertarDetalleCategoria(IN _idUsuario INT, IN _nombreCategoria VARCHAR(20))
		BEGIN
		DECLARE _idCategoria INT;
        
        INSERT INTO Categoria (nombreCategoria) VALUES(_nombreCategoria);
        
        SET _idCategoria = (SELECT MAX(idCategoria) FROM Categoria);
			INSERT INTO DetalleCategoria(idUsuario, idCategoria) 
				VALUES(_idUsuario, _idCategoria);
        END&&
DELIMITER ;
	
DELIMITER &&
	CREATE PROCEDURE sp_updateDetalleCategoria(IN _idCategoria INT, IN _nombreCategoria VARCHAR(20))
		BEGIN
			UPDATE Categoria SET nombreCategoria = _nombreCategoria
				WHERE idCategoria = _idCategoria;
                
            UPDATE DetalleCategoria SET idCategoria = _idCategoria
				WHERE idCategoria = _idCategoria;
                
        END&&
DELIMITER ;    

-- call sp_updateDetalleCategoria(2, 'nueva')

-- SELECT * FROM categoria

DELIMITER &&
	CREATE PROCEDURE sp_InsertarCita(IN _fecha DATETIME, IN _lugar VARCHAR(100), 
		IN _descripcion VARCHAR(100) , IN _idcontacto INT, IN _idUsuario INT)
		BEGIN
        DECLARE _idCita INT;
        
			INSERT INTO Cita(fecha, lugar, descripcion, idContacto)
				VALUES(_fecha, _lugar, _descripcion, _idcontacto);
        
        SET _idCita = (SELECT Max(idCita) FROM Cita);
        
			INSERT INTO DetalleCita(idUsuario, idCita)
				VALUES(_idUsuario, _idCita);
        
        END &&
DELIMITER ;

DELIMITER &&
	CREATE PROCEDURE sp_EditarCita(IN _fecha DATETIME, IN _lugar VARCHAR(100), 
		IN _descripcion VARCHAR(100), IN _idcontacto INT, IN _idUsuario INT, IN _idCita INT)
		BEGIN
			UPDATE Cita SET fecha = _fecha, lugar = _lugar, descripcion = _descripcion,
				idContacto = _idcontacto WHERE idCita = _idCita;
        
			UPDATE DetalleCita SET idCita = _idCita
				WHERE idUsuario = _idUsuario;
        
        END &&
DELIMITER ;
	
DELIMITER &&
    CREATE PROCEDURE sp_EliminarCita (IN _idCita INT)
    BEGIN
		DELETE FROM Cita WHERE idCita = _idCita;
    END&&
DELIMITER ;    

DELIMITER &&
	CREATE PROCEDURE sp_InsertarTarea(IN _idPrioridad INT, IN _nombre VARCHAR(100), IN _descripcion VARCHAR(100),
		IN _fecha DATETIME, IN _idUsuario INT)
    BEGIN
		DECLARE _idTarea INT;
		INSERT INTO Tarea(idPrioridad, nombre, descripcion, fecha) VALUES(_idPrioridad, _nombre, _descripcion, _fecha);
        SET _idTarea = (SELECT MAX(idTarea) FROM Tarea);
		INSERT INTO DetalleTarea(idTarea, idUsuario) 
			VALUES(idTarea, _idUsuario);
    END&&
DELIMITER ;

DELIMITER &&
	CREATE PROCEDURE sp_EditarTarea(IN _idTarea INT, IN _idPrioridad INT, IN _nombre VARCHAR(100), IN _descripcion VARCHAR(100), IN _fecha DATETIME)
	BEGIN
		UPDATE Tarea SET idTarea = _idTarea, idPrioridad = _idPrioridad, nombre = _nombre, descripcion = _descripcion, 
        fecha = _fecha WHERE idTarea = _idTarea;
            
		UPDATE DetalleTarea SET idTarea = _idTarea 
			WHERE idTarea = _idTarea;
        
    END&&
DELIMITER ;


DELIMITER &&
	CREATE PROCEDURE sp_EliminarTarea(IN _idTarea INT)
    BEGIN
		DELETE FROM Tarea WHERE idTarea = _idTarea;
    END&&

DELIMITER ;

DELIMITER $$

CREATE PROCEDURE sp_ActualizarUsuario (IN _idUsuario INT, IN _nick VARCHAR(30), IN _contrasena VARCHAR(30))
	BEGIN
    UPDATE Usuario SET nick = _nick, contrasena = _contrasena
    WHERE idUsuario = _idUsuario;
    END$$

DELIMITER ;

DELIMITER &&
CREATE PROCEDURE sp_EliminarUsuario(IN _idUsuario INT)
	BEGIN
    DELETE FROM Usuario WHERE idUsuario = _idUsuario;
    
    END&&
    
DELIMITER ;












DELIMITER &&
CREATE TRIGGER tr_historialContactoInsert	
	AFTER INSERT    
	ON DetalleUsuario FOR EACH ROW
    BEGIN
    INSERT INTO Historial(idUsuario, mensaje) 
		VALUES(NEW.idUsuario, 'Contacto creado');
    END&&
DELIMITER ;
    
    -- DROP TRIGGER tr_historialContactoInsert
    
DELIMITER &&
CREATE TRIGGER tr_historialContactoUpdate
	BEFORE UPDATE ON DetalleUsuario
    FOR EACH ROW
    BEGIN
		INSERT INTO Historial(idUsuario, mensaje)
			VALUES(NEW.idUsuario, 'Contacto actualizado');
    END&&
DELIMITER ;

DELIMITER &&
CREATE TRIGGER tr_historialContactoDelete
	BEFORE DELETE ON DetalleUsuario
	FOR EACH ROW
    BEGIN
    INSERT INTO Historial(idUsuario,  mensaje, fecha)
		VALUES(OLD.idUsuario, 'Contacto Eliminado');
    
    END&&
DELIMITER ;

DROP TRIGGER tr_historialContactoDelete

DELIMITER &&
CREATE TRIGGER tr_editarUsuario
	AFTER UPDATE ON Usuario
    FOR EACH ROW
    BEGIN
		INSERT INTO Historial(idUsuario, mensaje)
			VALUES(NEW.idUsuario, 'cambio de contraseña');
    END&&

DELIMITER ;

DELIMITER &&

CREATE TRIGGER tr_historialCitaInsert
	AFTER INSERT ON DetalleCita
    FOR EACH ROW
	BEGIN 
		INSERT INTO Historial(idUsuario, mensaje) 
			VALUES(NEW.idUsuario, 'cita creada');
    END&&
    
DELIMITER ;

DELIMITER &&
	CREATE TRIGGER tr_historialCitaUpdate
		AFTER UPDATE ON DetalleCita
			FOR EACH ROW
            BEGIN
				INSERT INTO Historial(idUsuario, mensaje)
					VALUES(NEW.idUsuario, 'cita editada');
            END&&
DELIMITER ;

DELIMITER &&
	CREATE TRIGGER tr_historialCitaDelete
		AFTER DELETE ON DetalleCita
		FOR EACH ROW
        BEGIN
			INSERT INTO Historial(idUsuario, mensaje)
				VALUES(OLD.idUsuario, 'cita eliminada');
        END&&
DELIMITER ;

DELIMITER &&

CREATE TRIGGER tr_historialTareaInsert
	AFTER INSERT ON DetalleTarea
    FOR EACH ROW
	BEGIN 
		INSERT INTO Historial(idUsuario, mensaje) 
			VALUES(NEW.idUsuario, 'tarea creada');
    END&&
    
DELIMITER &&

DELIMITER &&
	CREATE TRIGGER tr_historialTareaUpdate
		AFTER UPDATE ON DetalleTarea
			FOR EACH ROW
            BEGIN
				INSERT INTO Historial(idUsuario, mensaje)
					VALUES(NEW.idUsuario, 'tarea editada');
            END&&
DELIMITER ;

DELIMITER &&
	CREATE TRIGGER tr_historialTareaDelete
		AFTER DELETE ON Tarea
		FOR EACH ROW
        BEGIN
			INSERT INTO Historial(idUsuario, mensaje)
				VALUES(OLD.idUsuario, 'tarea eliminada');
        END&&
DELIMITER ;



-- call sp_insertarContacto(1,1,'a','a','a','a','a')
