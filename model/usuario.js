var database = require('./database/connection');

var usuario = {};

usuario.getUsuarios = function(callback) {
    if (database) {
        database.query('SELECT * FROM Usuario', 
        function(error, results) {
            if (error) {
                throw error;
            } else {
                callback(null, results);
            }
        });
    }
}

usuario.getUsuario = function(idUsuario, callback) {
    if (database) {
        var sql = 'SELECT * FROM Usuario WHERE idUsuario = ?';
        database.query(sql, idUsuario, 
        function(error, result) {
            if (error) {
                throw error;
            } else {
                callback(null, result);
            }
        });
    }
}

usuario.insert = function(data, callback) {
    if (database) {
        var sql = 'INSERT INTO Usuario (nick, contrasena, foto) '
        + 'VALUES ?';
        var values = [[data.nick, data.contrasena, data.foto]]
        database.query(sql, [values], 
        function(error, result) {
            if (error) {
                throw error;
            } else {
                callback(null, {'insertId': result.insertId});
            }
        });
    }
}

usuario.update = function(data, callback) {
    if (database) {
        var sql = 'UPDATE Usuario SET nick = ?, contrasena = ? WHERE '
        + 'idUsuario = ?';
        database.query(sql, [data.nick, data.contrasena, data.idUsuario], 
        function(error, result) {
            if (error) {
                throw error;
            } else {
                callback(null, {'resultId': result.insertId});
            }
        });
    }
}

usuario.delete = function(idUsuario, callback) {
    if (database) {
        var sql = 'DELETE FROM Usuario WHERE idUsuario = ?';
        database.query(sql, idUsuario, 
        function(error, result) {
            if (error) {
                throw error;
            } else {
                callback(null, {'Mensaje': 'Eliminado con exito'});
            }
        });
    }
}

usuario.autenticar = function(data,  callback) {
    if (database) {
        var sql = 'SELECT * FROM Usuario WHERE nick = ? AND contrasena = ?';
        database.query(sql, [data.nick, data.contrasena], 
        function(error, result) {
            if (error) {
                throw error;
            } else {
                console.log(result);
                callback(null, result);
            }
        });
    }
}

module.exports = usuario;