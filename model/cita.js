var database = require('./database/connection');
var cita = {};

cita.selectAll = function(idUsuario, callback) {
  if(database) {
    database.query("SELECT * FROM cita_usuario WHERE idUsuario = ?",
    idUsuario,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

cita.select = function(id, callback) {
  if(database) {
    var sql = "SELECT * FROM cita WHERE idCita = ?";
    database.query(sql, id,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

cita.insert = function(data, callback) {
  if(database) {
    database.query("call sp_InsertarCita(?,?,?,?,?); ", [data.fecha,data.lugar,
    data.descripcion,data.idContacto,data.idUsuario],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

cita.update = function(idUsuario, data, callback) {
  if(database) {
    var sql = "call set_cita(?,?,?,?,?, ?);";
    database.query(sql,
    [data.fecha, data.lugar, data.descripcion, data.idContacto, idUsuario, data.idCita],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {'insertId': resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

cita.delete = function(idCita, callback) {
  if(database) {
    var sql = "call eliminar_cita (?)";
    database.query(sql, idCita,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll


module.exports = cita;
