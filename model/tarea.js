var database = require('./database/connection');
var tarea = {};

tarea.selectAll = function(idUsuario, callback) {
  if(database) {
    database.query("SELECT * FROM tarea_usuario WHERE idUsuario = ?",
    idUsuario,
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

tarea.selectAllProridades = function(callback) {
  if(database) {
    database.query("SELECT * FROM prioridad",
    function(error, resultados) {
      if(error) {
        throw error;
      } else {
        callback(null, resultados);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

tarea.select = function(id, callback) {
  if(database) {
    var sql = "SELECT * FROM tarea WHERE idTarea = ?";
    database.query(sql, id,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, resultado);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

tarea.insert = function(idUsuario, data, callback) {
  if(database) {
    database.query("call sp_InsertarTarea(?,?,?,?,?); ", [data.idPrioridad, data.nombre, 
    data.descripcion, data.fecha, idUsuario],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"insertId": resultado.insertId});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

tarea.update = function(data, callback) {
  if(database) {
    var sql = "call sp_EditarTarea(?,?,?,?,?,?);";
    database.query(sql,
    [data.idTarea, data.idPrioridad, data.nombre, data.descripcion, data.fecha],
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, data);
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll

tarea.delete = function(id, callback) {
  if(database) {
    var sql = "call sp_EliminarTarea(?)";
    database.query(sql, id,
    function(error, resultado) {
      if(error) {
        throw error;
      } else {
        callback(null, {"Mensaje": "Eliminado"});
      }
    });//Fin query
  }//Fin IF
}//FIN SelectAll


module.exports = tarea;
