var database = require('./database/connection');

var contacto = {};

contacto.getContactos = function(idUsuario, callback) {
    if (database) {
        database.query('SELECT * FROM contacto_usuario WHERE idUsuario = ?', idUsuario, 
        function(error, results) {
            if (error) {
                throw error;
            } else {
                callback(null, results);
            }
        });
    }
}

contacto.getContacto = function(idContacto, callback) {
    if (database) {
        var sql = 'SELECT * FROM Contacto WHERE idContacto = ?';
        database.query(sql, idContacto, 
        function(error, result) {
            if (error) {
                throw error;
            } else {
                callback(null, result);
            }
        });
    }
}

contacto.insert = function(idUsuario, data, callback) {
    if (database) {        
        var sql = 'call sp_InsertarContacto(?, ?, ?, ?, ?, ?, ?)';
        var values = [idUsuario, data.idCategoria, data.nombre, data.apellido, 
            data.direccion, data.telefono, data.correo];
        database.query(sql, values, 
        function(error, result) {
            if (error) {
                console.log(error);
            } else {
                callback(null, {'insertId': result.insertId});
            }
        });
    }
}

contacto.update = function(data, callback) {
    if (database) {
        var sql = 'call sp_EditarContacto(?, ?, ?, ?, ?, ?, ?)';

        var values = [data.idContacto, data.idCategoria, data.nombre,
        data.apellido, data.direccion, data.telefono, data.correo];

        database.query(sql, values, 
        function(error, result) {
            if (error) {
                throw error;
            } else {
                callback(null, {'resultId': result.insertId});
            }
        });
    }
}

contacto.delete = function(idContacto, callback) {
    if (database) {
        var sql = 'call sp_EliminarContacto(?)';
        database.query(sql, idContacto, 
        function(error, result) {
            if (error) {
                throw error;
            } else {
                callback(null, result);
            }
        });
    }
}



module.exports = contacto;