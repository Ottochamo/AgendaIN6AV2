var database = require('./database/connection');

var historial = {};

historial.getHistorials = function(idUsuario, callback) {
    if (database) {
        console.log('hola')
        var sql = 'SELECT * FROM historial_usuario WHERE idUsuario = ?';
        database.query(sql, idUsuario, function(error, resultados) {
            if (error) {
                console.log(error)
                throw error;
            } else {
                callback(null, resultados);
            }
        });
    }
}

module.exports = historial;