function ViewModel() {
    var main = this;
    var citaUri = '/api/cita/';
    var contactoUri = '/api/Contacto';


    main.contactos = ko.observable();
    main.citas = ko.observableArray();
    main.citaCargada = ko.observable('');
    main.citaNueva = {
        'idContacto': ko.observable(),
        'fecha': ko.observable(),
        'lugar': ko.observable(),
        'descripcion': ko.observable()
    };

    main.agregar = function() {
        var data = {
            'idContacto': main.citaNueva.idContacto(),
            'fecha': main.citaNueva.fecha(),
            'lugar': main.citaNueva.lugar(),
            'descripcion': main.citaNueva.descripcion()
        }

        ajaxHelper(citaUri, 'POST', data).done(function() {
            main.getCitas();
        });
    }

    main.editar = function() {
        var data = {
            'idCita': main.citaCargada().idCita,
            'idContacto': main.citaCargada().idContacto,
            'fecha': main.citaCargada().fecha,
            'lugar': main.citaCargada().lugar,
            'descripcion': main.citaCargada().descripcion
        }

        var uri = citaUri + data.idCita;

        ajaxHelper(uri, 'PUT', data).done(function() {
            main.getCitas();
        })

    }

    main.eliminar = function(item) {
        var uri = citaUri + item.idCita;

        ajaxHelper(uri, 'DELETE').done(function() {
            main.getCitas();
        });

    }

    main.getCitas = function() {
        ajaxHelper(citaUri, 'GET').done(function(data) {
            main.citas(data);
        })
    }

    main.getContactos = function() {
        ajaxHelper(contactoUri, 'GET').done(function(data) {
            main.contactos(data);
        });
    }

    main.getCitas();
    main.getContactos();

}

function ajaxHelper(uri, method, data) {
    return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log('uri ' + uri);
            console.log('method' + method);
            console.log(errorThrown);
        });
}

$(document).ready(function() {
    ko.applyBindings(new ViewModel());
})