function ViewModel () {
    var usuarioUri = '/api/Usuario/';
    var main = this;

    main.usuarioCargado = ko.observable('');


    main.guardar = function() {
        var data = {
            'nick': main.usuarioCargado().nick,
            'contrasena': main.usuarioCargado().contrasena
        }
    }

    main.getCuenta = function() {
        var c = document.cookie.split(';')[0];
        var idUsuario = c.replace('idUsuario=', '');
        
        var uri = usuarioUri + idUsuario;

        ajaxHelper(uri, 'GET').done(function(data) {
            
            console.log(data[0]);
            main.usuarioCargado(data[0]);
        });

    }

    main.getCuenta();


}


function ajaxHelper(uri, method, data) {
    return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log('uri ' + uri);
            console.log('method' + method);
            console.log(errorThrown);
        }); 
}
$(document).ready(function() {
    ko.applyBindings(new ViewModel());
})