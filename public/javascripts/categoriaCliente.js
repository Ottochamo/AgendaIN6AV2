function ViewModel() {
    var main = this;
    var micategoriaUri = 'api/Categorias/';
    var categoriaUri = 'api/Categoria/';

    main.categorias = ko.observableArray();
    main.categoriaCargada = ko.observable('');

    main.categoriaNueva = {
        nombre: ko.observable()
    }

    main.agregar = function() {
        var data = {            
            nombreCategoria: main.categoriaNueva.nombre()
        }
        
        ajaxHelper(categoriaUri, 'POST', data).done(function() {
            main.getCategorias();
        })

    }

    main.editar = function() {
        var data = {
            idCategoria: main.categoriaCargada().idCategoria,
            nombreCategoria: main.categoriaCargada().nombre
        }

        var uri = categoriaUri + data.idCategoria;

        ajaxHelper(uri, 'PUT', data).done(function() {
            main.getCategorias();
        });

    }

    main.eliminar = function(item) {
        var uri = categoriaUri + item.idCategoria;

        ajaxHelper(uri, 'DELETE').done(function() {
            main.getCategorias();
        })

    }

    main.getCategorias = function() {
        ajaxHelper(micategoriaUri, 'GET').done(function (data) {
            main.categorias(data);
        });
    }

    main.getCategorias();
}

function ajaxHelper(uri, method, data) {
    return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log('uri ' + uri);
            console.log('method' + method);
            console.log(errorThrown);
        });
}
$(document).ready(function() {
    ko.applyBindings(new ViewModel());
});