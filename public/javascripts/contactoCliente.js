
var Contacto = function() {
    var main = this;
    var contactoUri = '/api/Contacto/';
    var categoriaUri = '/api/Categoria/';
    var historialUri = '/api/Historial/';

    main.contactoNuevo = {
        nombre: ko.observable(),
        apellido: ko.observable(),
        direccion: ko.observable(),
        correo: ko.observable(),
        telefono: ko.observable(),
        idCategoria: ko.observable()   
    };

    main.contactoCargado = ko.observable('');
    main.historials = ko.observableArray();
    main.contactos = ko.observable([]);
    main.categorias = ko.observable([]);

    main.getCategorias = function() {
        ajaxHelper(categoriaUri, 'GET').done(function (data) {
            main.categorias(data);
        });
    }

    main.getContactos = function() {
        ajaxHelper(contactoUri, 'GET').done(function(data) {            
            main.contactos(data);
        });
    }

    main.getHistorials = function() {
        ajaxHelper(historialUri, 'GET').done(function(data) {
            main.historials(data);
        });
    }

    main.cargar = function(element) {
        console.log(element)
        main.contactoCargado(element);
    }

    //CRUD Ops

    main.agregar = function() {
        var data = {
        nombre: main.contactoNuevo.nombre(),
        apellido: main.contactoNuevo.apellido(),
        direccion: main.contactoNuevo.direccion(),
        correo: main.contactoNuevo.correo(),
        telefono: main.contactoNuevo.telefono(),
        idCategoria: main.contactoNuevo.idCategoria()   
        };
        
        console.log(JSON.stringify(data))

        ajaxHelper(contactoUri, 'POST', data).done(function() {
            main.getContactos();
        });
    }

    main.editar = function() {
        var data = {
        idContacto: main.contactoCargado().idContacto,
        nombre: main.contactoCargado().nombre,
        apellido: main.contactoCargado().apellido,
        direccion: main.contactoCargado().direccion,
        correo: main.contactoCargado().correo,
        telefono: main.contactoCargado().telefono,
        idCategoria: main.contactoCargado().idCategoria   
        };
        
        var uri = contactoUri + data.idContacto;

        ajaxHelper(uri, 'PUT', data).done(function() {
            main.getContactos();
        });
    }

    main.eliminar = function(item) {    
        var uri = contactoUri + item.idContacto;
        $.when(ajaxHelper(uri, 'DELETE')).done(function () {  
            main.getContactos();
        });

    }

    //CRUD Ops

    main.getContactos();
    main.getCategorias();
    main.getHistorials();

    function ajaxHelper(uri, method, data) {
    return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function(jqXHR, textStatus, errorThrown) {
            console.log('uri ' + uri);
            console.log('method' + method);
            console.log(errorThrown);
        });
}


}

$(document).ready(function() {
    var contacto = new Contacto();
    ko.applyBindings(contacto);

});


/*
*PROCESOS ALMACENADOS
**CONTACTO
**USUARIO
*
*TRIGGERS
**HISTORIAL

APLICACION WEB
*MANEJO DE COOKIES
*CRUD DE CONTACTO
*CRUD DE USUARIO
*LINEA DE TIEMPO


*/